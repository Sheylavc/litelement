import { LitElement, html } from 'lit-element';

class PersonaForm extends LitElement {

    static get properties() {
        return {
            person: {type: Object},
            editingPerson: {type: Boolean}
        };
    }

    constructor() {
        super();
        //this.person = {};
        this.resetFormData();
    }

    render() {
        return html `
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

            <div>
                <form>
                    <div class="form-group">
                        <label>Nombre Completo</label>
                        <input @input="${this.updateName}" .value="${this.person.name}" ?disabled="${this.editingPerson}" type="text" id="personFormName" class="form-control" placeholder="Ingrese su nombre completo"/>
                    </div>
                    <div class="form-group">
                        <label>Perfil</label>
                        <textarea @input="${this.updateProfile}" .value="${this.person.profile}" class="form-control" placeholder="Ingrese brevemente su perfil" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa</label>
                        <input @input="${this.updateYearsInCompany}" .value="${this.person.yearsInCompany}" type="text" class="form-control" placeholder="Ingrese su antigüedad"/>
                    </div>
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>
        `;
    }

    goBack(e) {
        console.log("goBack");
        e.preventDefault();
        this.resetFormData();
        this.dispatchEvent(new CustomEvent("persona-form-close",{}));
    }

    updateName(e) {
        console.log("updateName");
        console.log("Actualizando la propiedad name con el valor " + e.target.value);
        this.person.name = e.target.value;
    }

    updateProfile(e) {
        console.log("updateProfile");
        console.log("Actualizando la propiedad profile con el valor " + e.target.value);
        this.person.profile = e.target.value;
    }

    updateYearsInCompany(e) {
        console.log("updateYearsInCompany");
        console.log("Actualizando la propiedad yearsInCompany con el valor " + e.target.value);
        this.person.yearsInCompany = e.target.value;
    }

    storePerson(e) {
        console.log("storePerson");
        e.preventDefault();

        this.person.photo = {
            "src": "./src/img/user.png",
            "alt": "Persona"
        };

        console.log("La propiedad name vale " + this.person.name);
        console.log("La propiedad profile " + this.person.profile);
        console.log("La propiedad yearsInCompany " + this.person.yearsInCompany);

        this.dispatchEvent(new CustomEvent("persona-form-store", {
            detail: {
                //person: {
                    name: this.person.name,
                    profile: this.person.profile,
                    yearsInCompany: this.person.yearsInCompany,
                    photo: this.person.photo,
                    editingPerson: this.editingPerson
                //}
            }
        }));

        this.resetFormData();
    }

    resetFormData() {
        console.log("resetFormData en persona-form");
        this.person = {};
        this.person.name = "";
        this.person.profile = "";
        this.person.yearsInCompany = "";

        this.editingPerson = false;
    }

}

customElements.define('persona-form', PersonaForm)