import{ LitElement, html } from 'lit-element';
class FichaPersona extends LitElement{
    static get properties(){
        return{
            name:  {type: String},
            yearsInCompany:  {type: Number},
            personalInfo:  {type: String},
            photo: {type: Object}
        };
        
    }

    constructor(){
        super();
        this.name = "Prueba";
        this.yearsInCompany = 12;
        this.photo = {
            src: "./src/img/user.png",
            alt: "foto persona"
        };
        this.personalInfo = "";
        this.updatePersonalInfo();
    }

    render(){
        return html `
            <div>Ficha Persona</div>
            <div>
                <label for="name">Nombre Completo</label>
                <input type="text" id="name" name="name" value="${this.name}" @input="${this.updateName}"></input>
                <br/>

                <label for="yearsInCompany">Años en la empresa</label>
                <input type="text" id="yearsInCompany" name="yearsInCompany" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"></input>
                <br/>
               
                <label for="personalInfo">Experiencia</label>
                <input type="text" id="personalInfo" name="personalInfo" value="${this.personalInfo}" disabled></input>
                <br/>
                
                <img src="${this.photo.src}" height="200" width="200" alt="${this.photo.alt}"></img>
            </div>
        `;
    }

    updated(changedProperties){
        if(changedProperties.has("name")){
            console.log("Propiedad name cambió su valor de "+changedProperties.get("name") + " a " + this.name);
        }
        if(changedProperties.has("yearsInCompany")){
            console.log("Propiedad yearsInCompany cambió su valor de "+changedProperties.get("yearInCompany") + " a " + this.yearsInCompany);
            this.updatePersonalInfo();
        }
        if(changedProperties.has("personalInfo")){
            console.log("Propiedad personalInfo cambió su valor de "+changedProperties.get("personalInfo") + " a " + this.personalInfo);
            this.updatePersonalInfo();
        }
    }

    updateName(e){
        console.log("updateName");
        this.name = e.target.value;
    }

    updatePersonalInfo(e){
        console.log("updatePersonalInfo");
        console.log("yearsInCompany " + this.yearsInCompany);
        if(this.yearsInCompany >= 7){
            this.personalInfo = "lead";
        }else if(this.yearsInCompany >= 5){
            this.personalInfo = "senior";
        }else if(this.yearsInCompany >= 3){
            this.personalInfo = "team";
        }else{
            this.personalInfo = "junior";
        }
    }
    updateYearsInCompany(e){
        this.yearsInCompany = e.target.value;
    }
}

customElements.define('ficha-persona', FichaPersona);